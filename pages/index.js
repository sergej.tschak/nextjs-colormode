import Head from 'next/head';
import React, { useState, useEffect } from 'react';

export default function Home() {
  const modes = {
    DARK_MODE: 'DARK MODE',
    LIGHT_MODE: 'LIGHT MODE'
  }

  const [colorMode, setColorMode] = useState(modes.LIGHT_MODE)

  // After render
  useEffect(() => localStorage.getItem('colorMode') ? setColorMode(localStorage.getItem('colorMode')) : null, [])

  // After mode change
  useEffect(() => localStorage.setItem('colorMode', colorMode), [colorMode])

  const getOppositeMode = () => (colorMode === modes.DARK_MODE) ? modes.LIGHT_MODE : modes.DARK_MODE
  const handleSwitchMode = () => setColorMode(getOppositeMode())
  const newColorMode = getOppositeMode()

  return (
    <div className={"container " + colorMode.toLowerCase().replace(' ', '-')} >
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1 className="title">
          Color Mode is <span>{colorMode.split(' ')[0]}</span>
        </h1>

        <div className="grid">
          <button href="https://nextjs.org/docs" className="mode-switch" onClick={() => handleSwitchMode()}>
            Switch to {newColorMode}
            <div className={"color-picker " + newColorMode.toLowerCase().replace(' ', '-')}></div>
          </button>
        </div>
      </main>

      <style jsx>{`
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        a,
        button {
          color: inherit;
          background-color: inherit;
          text-decoration: none;
        }

        button:hover {
          cursor: pointer;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: 4rem;
        }

        .title,
        .description {
          text-align: center;
        }

        .title span {
          color: #0070f3;
          text-decoration: none;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;

          max-width: 800px;
          margin-top: 3rem;
        }

        .mode-switch {
          margin: 1rem;
          flex-basis: 45%;
          padding: 1.5rem;
          text-align: left;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
          min-width: 250px;
          position: relative;
          outline: none;
        }

        .mode-switch:hover {
          color: #0070f3;
          border-color: #0070f3;
        }

        .color-picker {
          border-radius: 2rem;
          width: 2rem;
          height: 2rem;
          position: absolute;
          top: 1rem;
          right: 1rem;
        }

        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }

        .dark-mode {
          background-color: #1e1e1e;
          color: #ffffff;
        }

        .light-mode {
          background-color: #ffffff;
        }
      `}</style>
    </div>
  )
}
